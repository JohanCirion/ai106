import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.afcepf.ai106.artistes.Performeur;
import fr.afcepf.ai106.musique.Instrument;

public class App {
	
	public static void main(String[] args) 
	{
		//ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		ApplicationContext context = 
				new ClassPathXmlApplicationContext(new String[] {"instruments.xml", "musiciens.xml"});
		
		Performeur p = (Performeur)context.getBean("sonny1");
		p.perform();
		
//		Performeur p2 = (Performeur)context.getBean("SonnyJunior");
//		p2.perform();
//		System.out.println(p2.hashCode());
	}

}
