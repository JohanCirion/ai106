package fr.afcepf.ai106.musique;

import org.springframework.stereotype.Component;

@Component(value="guit")
public class Guitare implements Instrument {

	public void jouer()
	{
		System.out.println("gling gling");
	}

}
