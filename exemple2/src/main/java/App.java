import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.afcepf.ai106.artistes.Performeur;
import fr.afcepf.ai106.musique.Guitare;
import fr.afcepf.ai106.musique.Instrument;

public class App {
	
	public static void main(String[] args) 
	{
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//Instrument i = (Instrument)context.getBean("guit");
		//Instrument i = context.getBean(Instrument.class);
		
		Performeur p = context.getBean(Performeur.class);
		
		p.perform();
	}

}
